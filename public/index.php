<?php
/**
 * Created by PhpStorm.
 * User: Ashk
 * Date: 29/03/2018
 * Time: 22:08
 */

use App\Middlewares;

session_start();

require_once '../vendor/autoload.php';


//phpinfo();
//return;
function loadSettings() {
    $yaml = file_get_contents('../settings/settings.yml', true);

    $settings = [
        'settings' => yaml_parse($yaml)
    ];

    return $settings;
}

$app = new \Slim\App(loadSettings());

require_once '../app/Container.php';
require_once '../app/Routes.php';

$app->add(new Middlewares\CsrfViewMiddleware($container));
$app->add(new Middlewares\ValidationErrorsMiddleware($container));
$app->add(new Middlewares\OldInputsMiddleware($container));


//$app->add($container->csrf);
$app->run();
