function alert_flash(type, content) {
    switch (type) {
        case 'error':
        case 'danger':
            toastr_error(content);
            break;
        case 'success':
            toastr_success(content);
            break;
        case 'info':
            toastr_info(content);
            break;
    }
}

function toastr_error(content) {
    $.toast({
        heading: 'Notification!',
        text: content,
        position: 'top-right',
        loaderBg:'#ff6849',
        bgColor: '#f62d51',
        textColor: 'white',
        icon: 'error',
        hideAfter: 5000,
        stack: 6
    });

 }

function toastr_success(content) {
    $.toast({
        heading: 'Notification!',
        text: content,
        position: 'top-right',
        loaderBg:'#ff6849',
        bgColor: '#55ce63',
        textColor: 'white',
        icon: 'success',
        hideAfter: 5000,
        stack: 6
    });

}

function toastr_info(content) {
    $.toast({
        heading: 'Notification!',
        text: content,
        position: 'top-right',
        loaderBg:'#ff6849',
        bgColor: '#009efb',
        textColor: 'white',
        icon: 'info',
        hideAfter: 5000,
        stack: 6
    });
}