var isRegionActive = function(nb){
  console.log('nb = ', nb);
  for (var i = 0; i < available_regions.length; i++) {
    if (available_regions[i] == nb) {
        return true;
    }
  }
  return false;
}

$('#update_btn').click(function(){
  this.remove();
  var bar = $('<div class="progress-bar bg-success" style="width: 90%;height:15px;transition: width 2s;border: 1px solid black" role="progressbar">75%</div>');
  $('#btn_cont').prepend(bar);
  var nbval = 0;
  $.get('https://fr.openfisca.org/api/v21/variables').done(function(result){
    for (var i = 0; nbval < Object.keys(result).length; i++) {
      $.ajax({
        url: '/variables/update/' + parseInt(i * 100)+ '/' + 100,
        async: false,
        success: function(data) {
          nbval += 100;
          bar.css('width', (nbval / Object.keys(result).length) * 100 + "%");
          bar.html((nbval / Object.keys(result).length) * 100);
        },
        error: function(a, b, c) {console.log(a, b, c);}
      });
    }
    });
});

$(document).ready(function(){

  var width = 1560,
      height = 500,
      centered;

    var projection = d3.geoConicConformal()
    .center([20.454071, 44.279229])
    .scale(2100)
    .translate([width / 2, height / 2]);

  var path = d3.geoPath()
      .projection(projection);

  var svg = d3.select("svg#svg_map")
    .attr("preserveAspectRatio", "xMinYMin meet")
    .attr("viewBox", "0 0 600 400")
    .attr('svg_map', 'france')


  svg.append("rect")
      .attr("class", "background")
      .attr("width", '100%')
      .attr("height", '100%')
      .attr('fill', ' white')


  var g = svg.append("g");


  /*
  d3.json("assets/small-france.json", function(error, us) {
    if (error) throw error;

    g.append("g")
          .attr("id", "background-state")
        .selectAll("path")
        .data(us.features.reverse())
        .enter()
        .append("path")
        .attr("d", path)
        .attr('filter', 'url(#filter1)')
  });
*/
  d3.json("assets/france.json", function(error, us) {
    if (error) throw error;
    console.log(us);

    g.append("g")
        .attr("id", "states")
        .selectAll("path")
        .data(us.features)
        .enter()
        .append("path")
        .attr("d", path)
        .attr('fill', function(d) {
          if (isRegionActive(d.properties.CODE_DEPT)) {
            return '#007ebf';
          }
          return '#f2f7f8';
        })
        .on("click", clicked)



  /*  g.append("path")
        .datum(topojson.mesh(us, us.objects.states, function(a, b) { return a !== b; }))
        .attr("id", "state-borders")
        .attr("d", path);
        */
  });

  function clicked(d) {
    console.log(d);

  path = d3.geoPath()
      .projection(projection);

    var x, y, k;

    if (d && centered !== d) {
      var centroid = path.centroid(d);
      x = centroid[0] - 60
      y = centroid[1] - 35
      k = 4;
      centered = d;
    } else {
      x =  20.454071
      y = 44.279229
      k = 1;
      centered = null;
    }

    g.selectAll("path")
        .classed("active", centered && function(d) { return d === centered; });

    g.transition()
        .duration(750)
        .attr("transform", "translate(" + $('svg').width() / 2 + "," + $('svg').height() / 2 + ")scale(" + k + ")translate(" + -x + "," + -y + ")")
        .style("stroke-width", 1.5 / k + "px");
  }
});
