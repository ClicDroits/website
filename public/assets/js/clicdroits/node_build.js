var collapse = function(d) {
    if (d._children) {
        d.children = d._children;
        d.children.forEach(collapse);
        d._children = null;
    }
}

var get_db_info_json = function (aide) {
    var info ;
    $.ajax({
        type: "GET",
        url: "/json/variable/" + aide,
        async: false,
        success: function(resp){info = resp},
        error: function(a,b,c){$('html').html(a.responseText);}
    });
    return info;
}


var close_children = function(d) {
	if (d.children) {
        d._children = d.children;
        d.children = null;
    }
}
var swap_children = function(d) {
    if (d.children) {
        d._children = d.children;
        d.children = null;
    }
    else {
        d.children = d._children;
        d._children = null;
    }
}

var node_is_set = function(node) {
    if(selected.infos[node.name][fill]) {
        return 1;
    }
    return 0;
}


var exist_in_json = function (name, json) {
    var add = 0;
    if (json.name == name && (json.children != null || json._children != null)) {
        return 1;
    }
    if (json.children) {
        for (var i = 0, len = json.children.length; i < len; i++) {
            add += exist_in_json(name, json.children[i])
        }
    }
    if (json._children) {
        for (var i = 0, len = json._children.length; i < len; i++) {
            add += exist_in_json(name, json._children[i])
        }
    }
    return add;
}

var add_in_json = function(name, json, p) {
  if (name == "") {
    return;
  }
    if (json.name == name) {
      var data ;
      $.ajax({
          type: "GET",
          url: "/json/variable/" + name,
          async: false,
          success: function(resp){data = resp},
          error: function(a,b,c){$('html').html(a.responseText);}
      });
		var alldata = JSON.parse(data);
    if(!alldata || !data)
      return;
		data = JSON.parse(alldata.json);
    console.log(data);
		if (data.children && json.children == null && data.children.length > 0) {
			json.children = [];
			for (var i = 0, len = data.children.length; i < len; i++) {
				json.children.push(data.children[i]);
			}
		}
        return 1;
    }
    if (json.children) {
        for (var i = 0, len = json.children.length; i < len; i++) {
            if (add_in_json(name, json.children[i], p) == 1) {
                return 1;
            }
        }
    }
    return 0;
}

var node_can_be_calculated = function(d, v) {
    var c = d.children ? d.children : d._children;
    if (c) {
        for (var i = 0, len = c.length; i < len; i++) {
            if (c[i] && c[i].name && !(v.infos[c[i].name].calculate || v.infos[c[i].name].fill)) {
                return 0;
            }
        }
    }
    else {
        return 0
    }
    if (d.parent == "ROOT") {
        alert_flash('success', "La simulation peut etre lancer.");
    }
    return 1;
}

var node_calculate = function(d) {
    var c = d.children ? d.children : d._children;
    if (selected.infos[d.name][fill]) {
        return 1;
    }
    if (c) {
        for (var i = 0, len = c.length; i < len; i++) {
            if (node_calculate(c[i]) == 0) {
                return 0;
            }
        }
        return 1;
    }
    return 0;
}

var update_node = function(d) {
    if (node_can_be_calculated(d)) {
        d.iscalculate = 1;
        d._children = d.children;
        d.children = null;
    }
}


var expand  = function (d){
    var children = (d.children)?d.children:d._children;
    if (d._children) {
        d.children = d._children;
        d._children = null;
    }
    if(children)
        children.forEach(expand);
}

var expandAll = function(){
    expand(root);
}

var get_json_from_nodes = function (node) {
    if (node._children == null && node.children == null) {
        return {
            "name": node.name,
            "parent": node.parent.name
        };
    }
    else {
        node.tb = Array(0);
        if (node.children) {
            for (var i = 0; i < node.children.length; i++) {
                node.tb.push(get_json_from_nodes(node.children[i]))
            }
        }
        if (node._children) {
            for (var i = 0; i < node.children.length; i++) {
                node.tb.push(get_json_from_nodes(node._children[i]))
            }
        }
        return {
            "name": node.name,
            "parent": node.parent.name,
            "children": node.tb
        }
    }
}
