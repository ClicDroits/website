<?php
/**
 * Created by PhpStorm.
 * User: Ashk
 * Date: 30/03/2018
 * Time: 00:53
 */

namespace App\Controllers;

use Interop\Container\Exception\ContainerException;
use Psr\Http\Message\ResponseInterface;
use Slim\Container as Container;

class Controller
{

    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function render(ResponseInterface $response, $file, $datas=[])
    {
      return  $this->view->render($response, $file, $datas);
    }

    public function redirect(ResponseInterface $response, $name)
    {
        return $response->withStatus(302)->withHeader('Location', $this->router->pathFor($name));

    }

    public function __get($name)
    {
        try {
            return ($this->container->get($name));
        } catch (ContainerException $e) {
            return ($e->getMessage());
        }
    }
}
