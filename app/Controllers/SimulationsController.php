<?php
/**
 * Created by PhpStorm.
 * User: Ashk
 * Date: 30/03/2018
 * Time: 00:19
 */

namespace App\Controllers;


use App\Models\Algorithm\Algorithm;
use App\Models\Routines\RoutineLinkSimulation;
use App\Models\Simulations\Simulation;
use Slim\Http\Request;
use Slim\Http\Response;

class SimulationsController extends Controller {

    /* Simulations */
    public function getSimulations(Request $request, Response $response) {
        $simulations = Simulation::all();

        $this->render($response, 'pages/simulations/simulations.twig', ['simulations' => $simulations]);
    }

    /* Add Simulations */
    public function getAddSimulation(Request $request, Response $response) {
        $this->render($response, 'pages/simulations/add.twig');
    }

    public function postAddSimulation(Request $request, Response $response) {
        $name = $request->getParam('name');
        $description = $request->getParam('description');
        $dataset = $request->getParam('dataset');
        $type = $request->getParam('algorithm');

        $simulation = Simulation::create([
            'name' => $name,
            'description' => $description,
            'id_algorithm' => Algorithm::findByName($type)->id,
            'dataset' => $dataset
        ]);

        $this->notifications->addNotification("create", "Création d'une nouvelle simulation", "Simulation : $simulation->name");
        $this->flash->addMessage('success', 'La simulation ' . $simulation->name . ' a bien été créée!');

        return $response->withRedirect($this->router->pathFor('simulations'));
    }

    /* Simulation N */
    public function getSimulation(Request $request, Response $response, $args = null) {
        $id = $request->getAttribute('id');
        $simulation = Simulation::find($id);

        $this->render($response, 'pages/simulations/simulation.twig', ['simulation' => $simulation]);
    }

    public function postSimulation(Request $request, Response $response, $args = null) {
        $id = $request->getAttribute('id');
        $name = $request->getParam('name');
        $description = $request->getParam('description');
        $type = $request->getParam('algorithm');
        $dataset = $request->getParam('dataset');

        $simulation = Simulation::where('id', $id)->first();
        $simulation->update([
            'name' => $name,
            'description' => $description,
            'id_algorithm' => Algorithm::findByName($type)->id,
            'dataset' => $dataset
        ]);

        $this->notifications->addNotification("update", "Modification d'une simulation", "Simulation : $simulation->name");
        $this->flash->addMessage('success', 'La simulation a été mis à jour!');

        return $response->withRedirect($this->router->pathFor('simulations'));
    }

    public function getDeleteSimulation(Request $request, Response $response, $args = null) {
        $id = $request->getAttribute('id');
        $simulation = Simulation::where('id', $id)->first();

        $this->notifications->addNotification("delete", "Suppression d'une simulation", "Simulation : $simulation->name");
        $this->flash->addMessage('success', 'La simulation ' . $simulation->name . ' a bien été supprimée!');

        $this->db->getConnection()->beginTransaction();
        $simulation->results()->each(function ($item) {
            $item->link()->delete();
        });
        $this->db->getConnection()->commit();

        $simulation->delete();

        return $response->withRedirect($this->router->pathFor('simulations'));
    }

}