<?php
/**
 * Created by PhpStorm.
 * User: Ashk
 * Date: 01/04/2018
 * Time: 16:36
 */

namespace App\Middlewares;

use Slim\Http\Request;
use Slim\Http\Response;

class CsrfViewMiddleware extends Middleware
{
    public function __invoke(Request $request, Response $response, $next)
    {
        $csrfNameKey = $this->container->csrf->getTokenNameKey();
        $csrfValueKey = $this->container->csrf->getTokenValueKey();
        $csrfName = $request->getAttribute($csrfNameKey);
        $csrfValue = $request->getAttribute($csrfValueKey);
        $this->container->view->getEnvironment()->addGlobal('csrf', [
            'field' => '
                <input type="hidden" name="' . $csrfNameKey . '" 
                    value="' . $csrfName . '">
                <input type="hidden" name="' . $csrfValueKey . '" 
                    value="' . $csrfValue . '">
            ',
        ]);
        $response = $next($request, $response);
        return $response;
    }
}