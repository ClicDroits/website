<?php
/**
 * Created by PhpStorm.
 * User: Ashk
 * Date: 01/04/2018
 * Time: 01:53
 */

namespace App\Middlewares;

use Slim\Http\Request;
use Slim\Http\Response;

class ValidationErrorsMiddleware extends Middleware
{
    public function __invoke(Request $request, Response $response, $next)
    {
        if (isset($_SESSION['errors']))
            $this->container->view->getEnvironment()->addGlobal('errors', $_SESSION['errors']);
        unset($_SESSION['errors']);
        $response = $next($request, $response);
        return $response;
    }
}