<?php
/**
 * Created by PhpStorm.
 * User: Ashk
 * Date: 31/03/2018
 * Time: 20:03
 */

namespace App\Models\Simulations;

use App\Models\Algorithm\Algorithm;
use App\Models\Routines\RoutineActionLinkSimulationResult;
use Illuminate\Database\Eloquent\Model;

class SimulationResult extends Model {
    protected $table = 'simulations_results_template';
    protected $fillable = ['id_simulation', 'id_non_recourant', 'value', 'date', 'status', 'missing'];
    public $timestamps = false; #disabled updated/created_at

    public function simulation() {
        return $this->hasOne(Simulation::class, 'id', 'id_simulation');
    }

    public function link() {
        return $this->hasOne(RoutineActionLinkSimulationResult::class, 'id_simulation_result', 'id');
    }

}
