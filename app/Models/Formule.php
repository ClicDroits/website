<?php
/**
 * Created by PhpStorm.
 * User: Ashk
 * Date: 31/03/2018
 * Time: 20:03
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Formule extends Model {
    protected $table = 'formule_template';
    protected $primaryKey = 'id';
    public $incrementing = true;
    protected $fillable = ['id', 'name', 'formula', 'children', 'date'];
    public $timestamps = false; #disabled updated/created_at
}
