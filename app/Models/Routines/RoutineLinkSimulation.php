<?php
/**
 * Created by PhpStorm.
 * User: Ashk
 * Date: 31/03/2018
 * Time: 20:03
 */

namespace App\Models\Routines;

use App\Models\Simulations\Simulation;
use Illuminate\Database\Eloquent\Model;

class RoutineLinkSimulation extends Model {
    protected $table = 'routines_links_simulations_template';
    protected $fillable = ['id_routine', 'id_simulation'];
    public $timestamps = false; #disabled updated/created_at

}