<?php
/**
 * Created by PhpStorm.
 * User: Ashk
 * Date: 31/03/2018
 * Time: 20:03
 */

namespace App\Models;

use App\Models\Algorithm\Algorithm;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model {
    protected $table = 'notifications_template';
    protected $fillable = ['type', 'title', 'content', 'publish'];
    public $timestamps = false; #disabled updated/created_at

}