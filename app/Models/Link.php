<?php
/**
 * Created by PhpStorm.
 * User: Ashk
 * Date: 31/03/2018
 * Time: 20:03
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Link extends Model {
    protected $table = 'link_variable';

    protected $fillable = ['aide', 'variable', 'status', 'importance', 'default'];
    public $timestamps = false; #disabled updated/created_at
}
