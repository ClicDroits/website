<?php
/**
 * Created by PhpStorm.
 * User: Ashk
 * Date: 31/03/2018
 * Time: 20:03
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Formule;

class Variable extends Model {
    protected $table = 'variables_template';
    protected $primaryKey = 'name';
    public $incrementing = false;
    protected $fillable = ['name', 'valueType', 'entity', 'enum', 'link', 'description'];
    public $timestamps = false; #disabled updated/created_at

    public function get_last_link() {
      $formule = new Formule();
      //dd($this);
      if ($this !== null) {
      $lk = $formule->where('name', $this->getAttributes()['name'])->orderBy('date', 'DESC')->first();
      if ($lk) {
        $lk = $lk->getAttributes();
      }
      $data = $this->getAttributes();
      $data['child'] = $lk;
      return $data;
      }
    }
}
