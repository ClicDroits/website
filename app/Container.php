<?php
/**
 * Created by PhpStorm.
 * User: Ashk
 * Date: 30/03/2018
 * Time: 00:27
 */

$container = $app->getContainer();

$container['db'] = function ($container) {
    $capsule = new Illuminate\Database\Capsule\Manager();
    $capsule->addConnection($container['settings']['db']);
    $capsule->setAsGlobal();
    $capsule->bootEloquent();

    return $capsule;
};

$container['cookies'] = function ($container) {
    return new \App\Tools\Cookies($container);
};

$container['csrf'] = function ($container) {
    return new Slim\Csrf\Guard();
};

$container['flash'] = function ($container) {
    return new \Slim\Flash\Messages();
};

$container['notifications'] = function ($container) {
    return new \App\Tools\Notifications($container);
};

$container['view'] = function ($container) {
    $dir = dirname(__DIR__);
    $view = new \Slim\Views\Twig($dir . '/app/Views', [
        'cache' => false //$dir . '/tmp/cache'
    ]);

    $basePath = rtrim(str_ireplace('index.php', '', $container['request']->getUri()->getBasePath()), '/');

    $view->addExtension(new Slim\Views\TwigExtension(
        $container['router'],
        $basePath
    ));

    $view->getEnvironment()->addGlobal('flash', $container->flash);


    return $view;
};

$container['notFoundHandler'] = function ($container) {
    return function ($request, $response) use ($container) {
        return $container['view']->render($response->withStatus(404), 'pages/errors/404.twig');
    };
};

try {
    $container->get('db');
} catch (\Psr\Container\NotFoundExceptionInterface $e) {
} catch (\Psr\Container\ContainerExceptionInterface $e) {
    return ($e->getMessage());
}
