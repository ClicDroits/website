<?php
/**
 * Created by PhpStorm.
 * User: Ashk
 * Date: 23/04/2018
 * Time: 16:06
 */

namespace App\Tools;

use Slim\Container as Container;
use Interop\Container\Exception\ContainerException;

class Tool
{
    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function __get($name) {
        try {
            return ($this->container->get($name));
        } catch (ContainerException $e) {
            return ($e->getMessage());
        }
    }
}