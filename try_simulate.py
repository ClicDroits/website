from openfisca_france import CountryTaxBenefitSystem
from openfisca_core.simulations import Simulation
import time
import sys
import json
import datetime

if __name__ == "__main__":
    prev_mouth = datetime.date.today() + datetime.timedelta(-0*365/12)
    tax_benefit_system = CountryTaxBenefitSystem()
    simulation = Simulation(tax_benefit_system = tax_benefit_system, simulation_json = json.loads(sys.argv[2]))
    print simulation.calculate(sys.argv[1], prev_mouth.strftime('%Y-%m'))
