import yaml

class Settings:
    path = "../settings/settings.yml"
    load = ''

    def __init__(self):
        with open(self.path, 'r') as stream:
            try:
                self.load = yaml.load(stream)
            except yaml.YAMLError as exc:
                print(exc)

if __name__ == '__main__':
    conf = Settings().load
    print(conf)
