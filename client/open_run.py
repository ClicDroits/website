from openfisca_france import CountryTaxBenefitSystem
from openfisca_core.simulations import Simulation
from Settings import Settings
from CafSituationGenerator import *
import sys
import random
import requests
import subprocess


def get_data(file):
    f = get_exec('cat '+file+'|  tail -n $(expr 1 - $(cat '+file+'|wc -l)) | sed  \'s/;/,/g\'')
    data = f.split('\n')
    arr = []
    for line in data:
        arr.append(line.split(','))
    return arr

def get_exec(cmd):
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
    (output, err) = p.communicate()
    p_status = p.wait()
    return output

def send_result(id_routine, action):

    conf = Settings().load
    datasend = {'id' : id_routine, 'action' : action}
    datasend['results'] = results
    datasend  = json.dumps(datasend);
    p = requests.post(conf['baseUrl'] + '/routine/{}/action/{}/done'.format(id_routine, action), data={'data' : datasend})
    print p.content

def run(epoch, aides, data):
    fr_sys = CountryTaxBenefitSystem()
    prev_mouth = datetime.date.today() + datetime.timedelta(-1*(365/12))
    aide = 'cf'
    r = random.random()
    arr = [ random.choice(range(epoch)) for _ in range(int(sys.argv[1]))]
    aa = {'af' : 2, 'cf' : 3, 'alf' : 4}
    for _id in range(int(sys.argv[1])):
        situation = CafSituationGenerator(_id, aide, data)
        simulation = Simulation(tax_benefit_system = fr_sys, simulation_json = situation.get_json())
        for aide in aides:
            res = simulation.calculate(aide, prev_mouth.strftime('%Y-%m'))
            situation.check_and_print_result(res[0], aa[aide], aide)

if len(sys.argv) >= 6:
    run(int(sys.argv[1]), [sys.argv[d] for d in range(5, len(sys.argv))], get_data (sys.argv[2]))
    if int(sys.argv[3]) > -1:
        send_result(sys.argv[3], sys.argv[4])
