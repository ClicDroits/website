#! /usr/bin/python
# -*- coding:utf-8 -*-

#from openfisca_france import CountryTaxBenefitSystem
#from openfisca_core.simulations import Simulation
from flask import Flask, url_for, request
#from flask import render_template
from Database import Database 
from inspect import getmembers
import string
from pprint import pprint
import os
import json
import datetime
import random


db = Database()
prev_mouth = datetime.date.today();

def gen_birth_adult():
    return datetime.date.today() + datetime.timedelta(-random.uniform(20, 90) *365)

def gen_birth_child():
    return datetime.date.today() + datetime.timedelta(-random.uniform(1, 18) *365)

def off(n):
    return (datetime.date.today() + datetime.timedelta(-n * 365 / 12)).strftime("%Y-%m-%d")

def gen_string(le = 10):
    return ''.join(random.choice(string.ascii_uppercase) for _ in range(0, le))

def gen_rev(id_p):
    aide = random.uniform(200, 500) if random.uniform(0, 100) > 75 else 0
    rev = random.uniform(600, 2400) if random.uniform(0, 100) > 25 else 0
    name = random.choice(["rsa", "aah", "asf", "paje"])
    job = "JOB{" + gen_string() + "}"
    for i in range(0, 24):
        db.insert('revenu', { "id_parent" : id_p, "name" : job, "montant" : rev * random.uniform(0.9, 1.2), "date" : off(i)})
        db.insert('revenu', { "id_parent" : id_p, "name" : name , "montant" : aide, "date": off(i)})

def gen_individu(type):
    if (type == "ENFANT"):
        ndate = gen_birth_child().strftime("%Y-%m-%d")
    else:
        ndate = gen_birth_adult().strftime("%Y-%m-%d")

    db.insert('dos', {
        "firstname" : gen_string(),
        "lastname" : gen_string(), 
        "date_naissance" : ndate,
        "email" : gen_string() + "@gmail.com"
        })
    iid = db.lastId()
    if (type != "ENFANT"):
        gen_rev(iid)
    return iid;

def gen_famille():
    db.insert('menage', {"ref" : gen_individu("DARON"),
            "conjoint" : gen_individu("DARONNE"),
            "loyer" : random.uniform(400, 1500),
            "enfants" : ':'.join(str(gen_individu("ENFANT")) for _ in range(0, int(random.uniform(0, 5))))
        })
    db.commit()

def build_individu(id):
    indiv = db.getOne('dos', where=["id=" + str(id)])
    print indiv

if __name__ == '__main__':
    gen_famille();

