from simplemysql import SimpleMysql
from Settings import Settings

class Database(SimpleMysql):
    conf = Settings().load
    def __init__(self):
        SimpleMysql.__init__(self,
            host = self.conf['db']['host'],
            db = self.conf['db']['databaseBack'],
            user = self.conf['db']['username'],
            passwd = self.conf['db']['password'],
            keep_alive = True
        )

