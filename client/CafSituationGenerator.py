import os
import importlib
import string;
from Database import Database
import json
import time
import random;
import sys
import datetime

# 1 SituationGenerator par admin

db = Database()
data_folder = '1526380210_caf';

CEND = '\033[0m'
CRED    = '\033[91m'
CGREEN  = '\033[32m'
error_log = {}
results = []

def print_error():
    print(json.dumps(error_log, indent=2, sort_keys=True))

def rand_string(len):
    return ''.join(random.choice(string.ascii_lowercase) for _ in range(0, 10))

def repeat_value_month(val, datefrom, len):
    obj = {}
    for i in range(0, len):
        date = datefrom + datetime.timedelta(-i * (365/12))
        obj[date.strftime('%Y-%m')] = val
    return obj

def repeat_value_year(val, datefrom, len):
    obj = {}
    for i in range(0, len):
        date = datefrom + datetime.timedelta(-i * (365))
        obj[date.strftime('%Y')] = val
    return obj

def add_error(id, prop):
    if not str(id) in error_log:
        error_log[str(id)] = [prop]
    else:
        error_log[str(id)].append(prop)




class CafSituationGenerator(object):

    """Docstring for SituationGenerator. """

    def __init__(self, id, aide, data):
        self.aide = aide;
        self.data = data
        assert self.data != None
        id_m = rand_string(6);
        self.data = self.data[id]
        self.id = id;
        #self.indiv = [{'id': id, 'famille' : id_m, 'foyer_fiscal' : id_m , 'menage' : id_m}]
        self.modules = {}
        self.json = self.load_basic_situation();
        self.load_situation()



    def add_prop(self, prop, value, errValue, moy, repeat, repeat_over):
        value = self.data[value]
        if value and value != '':
            value = int(value)
        else:
            add_error(self.id, prop)
            return {}
        #print errValue, value
        if errValue != None and value == errValue:
            add_error(self.id, prop)
            return {};
        if moy:
            value /= moy
        if repeat_over == 'MOUTH':
            return repeat_value_month(value, datetime.date.today(), repeat)
        else:
            return repeat_value_year(value, datetime.date.today(), repeat)

    def add_prop_bi(self, prop, n1, n2, errValue, moy, repeat, repeat_over):
        if self.data[63] != '':
            return self.add_prop(prop, n2, errValue, moy, repeat, repeat_over)
        return self.add_prop(prop, n1, errValue, moy, repeat, repeat_over)

    def load_basic_situation(self):
        return {"individus" : {}, "familles" : {}, "foyers_fiscaux" : {}, "menages" : {}};

    def add_id(self, newid, famille, foyer, menage):
        if newid == '':
            return
        newid = int(newid)
        for id in self.indiv:
            if newid == id['id']:
                return
        self.indiv.append({'id': str(newid), 'famille' : str(famille), 'foyer_fiscal' : str(foyer) , 'menage' : str(menage)});


    ## Construction de la situation pour un id donner
    def load_situation(self):
        (self.ref, self.conj, self.enf) = self.get_situation()
        self.build_entity('familles', self.data[0], self.ref, self.conj, self.enf, 'parents', 'enfants')
        self.build_entity('foyers_fiscaux', self.data[0], self.ref, self.conj, self.enf, 'declarants', 'personnes_a_charge')
        self.build_menage(self.data[0], self.ref, self.conj, self.enf, self.data[1])
        self.build_ressource(self.ref)
        self.build_alf()

    def build_alf(self):
        self.json['familles'][str(self.data[0])]['al_nb_personnes_a_charge'] = repeat_value_month(self.data[7], datetime.date.today(), 24)
        self.json['familles'][str(self.data[0])]['paje_base'] = repeat_value_month(self.data[10], datetime.date.today(), 24)


    def build_ressource(self, ref):
        self.json['individus'][ref]['salaire_de_base'] = self.add_prop_bi('salaire_de_base', 14, 33, None, 12, 24, 'MONTH')
        self.json['individus'][ref]['chomage_imposable'] = self.add_prop_bi('chomage_imposable', 19, 34, None, 12, 24, 'MONTH')
        self.json['individus'][ref]['pensions_invalidite'] = self.add_prop_bi('pensions_invalidite', 29, 50, None, 12, 24, 'MONTH')
        self.json['foyers_fiscaux'][str(self.data[0])]['retraite_titre_onereux_net'] = self.add_prop_bi('retraite_titre_onereux_net', 20, 37, None, 12, 24, 'MONTH')
        self.json['individus'][ref]['pensions_alimentaires_percues'] = self.add_prop('pensions_alimentaires_percues', 9, None, 12, 24, 'MONTH')
        self.json['individus'][ref]['pensions_invalidite'] = self.add_prop_bi('pensions_invalidite', 29, 50, 0, None, 24, 'YEAR')
        self.json['foyers_fiscaux'][str(self.data[0])]['fon'] = self.add_prop_bi('fon', 21, 49, None, 12, 24, 'MONTH')
        #retraite ?

    def get_situation(self):
        date = datetime.datetime.strptime(self.data[5], '%d/%m/%Y')
        ref = self.gen_indiv(date.strftime('%Y-%m-%d'))
        conj = None
        if self.data[6]:
            date = datetime.datetime.strptime(self.data[6], '%d/%m/%Y')
            conj = self.gen_indiv(date.strftime('%Y-%m-%d'))
        if self.data[7] > 0:
            enf = [self.gen_enfants() for _ in range(0, int(self.data[7]))]
        return ref, conj, enf


    def build_menage(self, id, ref, conj, enf, depcom):
        self.gen_basic_entity('menages', id)
        self.json['menages'][str(id)]['personne_de_reference'] = [ref]
        if conj:
            self.json['menages'][str(id)]['conjoint'] = [conj]
        if enf and len(enf):
            self.json['menages'][str(id)]['enfants'] = enf
        self.json['menages'][str(id)]['depcom'] = repeat_value_month(depcom, datetime.date.today(), 24)

    def check_and_print_result(self, val2, nb, aide):
        val = val2 > 0
        #print self.data[nb]
        if int(val) == 1 and int(self.data[nb]) == 1:
            a =  CGREEN + "Match" + CEND
            st = 1
        elif int(val) == 1 and int(self.data[nb]) == 0:
            a =  CRED + "Faux positif" + CEND
            st = 4
        elif int(val) == 0 and int(self.data[nb]) == 1:
            a =  CRED + "Manquer" + CEND
            st = 3
        else:
            a =   CGREEN + "Match 0" + CEND
            st = 2
        results.append({'id_non_recourant' : self.id, 'aide' : aide, 'value' : int(val2),
        'test' : self.data[nb],
        'status': st,
        'missing': ':'.join( data for data in error_log[str(self.id)])})


        print "%4d : [%17s] [%5s] val = %7.2f AFVERS = %d" % (self.id, a, aide, float(val2), int(self.data[nb]))
        sys.stdout.flush()

    def build_entity(self, entity, id, ref, conj, enf, st, rd):
        self.gen_basic_entity(entity, id)
        self.json[entity][str(id)][st] = [ref]
        if conj:
             self.json[entity][str(id)][st].append(conj)
        if enf and len(enf):
            self.json[entity][str(id)][rd] = enf



    def gen_basic_entity(self, ent, id):
        self.json[ent][str(id)] = {};

    def gen_enfants(self):
        date = datetime.date.today() + datetime.timedelta(-random.randint(5, 18)*365)
        #date = datetime.date.today() + datetime.timedelta(44*365)
        name = self.gen_indiv(date.strftime('%Y-%m-%d'))
        self.json['individus'][name]['prestations_familiales_enfant_a_charge'] = repeat_value_month(1, datetime.date.today(), 24)
        self.json['individus'][name]['cf_enfant_a_charge'] = repeat_value_month(1, datetime.date.today(), 24)
        return name

    def gen_indiv(self, date):
        if not date:
            return None
        name = rand_string(6)
        self.json['individus'][name] = {'date_naissance' : {'ETERNITY' : date}}
        return name

    ## 2eme etape build chaque individu
    def build_individu(self, id):
        prev_month = datetime.date.today() + datetime.timedelta(-random.randint(18, 60)*365)
        self.json['individus'][str(id)] = {'date_naissance' : {'ETERNITY' : prev_month.strftime("%Y-%m-%d")}}
        #for file in os.listdir("../client/" + self.aide):
        #    if file.endswith(".py") and file != "__init__.py":
        #        print(file)#self.build_module(importlib.import_module(data_folder + "." + file.split('.')[0]), self.get_indiv(id))

    def build_module(self, module, indiv):
        if not module or not indiv:
            return
        class_module = getattr(module, module.desc["name"] + "_loader")();
        data = {}
        rreq = db.query(v.format(indiv['id']))
        for k, v in class_module.sql_req.items():
            data[k] = rreq[v];
        if (module.desc["entity"] == 'famille'):
            self.json['familles'][indiv['famille']][module.desc["name"]] = class_module.load(data);
        elif (module.desc["entity"] == 'foyer_fiscal'):
            self.json['familles'][indiv['foyer_fiscal']][module.desc["name"]] = class_module.load(data);
        elif (module.desc["entity"] == 'menage'):
            self.json['familles'][indiv['menage']][module.desc["name"]] = class_module.load(data);
        elif (module.desc["entity"] == 'individu'):
            self.json['individus'][str(indiv['id'])][module.desc["name"]] = class_module.load(data);

    def get_json(self):
        return self.json

    def print_json(self):
        print(json.dumps(self.json, indent=4, sort_keys=True))
